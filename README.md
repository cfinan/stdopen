# Getting Started with stdopen

__version__: `0.2.0a0`

This is a wrapper around Python's `open` (or other open methods), that will automate file-like object creation either by opening a file or from STDIN/STDOUT. It also provides the option writing to a temp file which is seamlessly copied over to the desired output directory upon successful closing, or deleted upon error. It will also pass through (back) any iterators.

All of the functionality described above is particulay useful when implementing cmd-line programs.

There is [online](https://cfinan.gitlab.io/stdopen) documentation for stdopen.

## Installation instructions

### pip install
At present, no packages exist yet on PyPy. Therefore, for pip install you should clone this repository and then `cd` to the root of the repository.

```
# Change in your package - this is not in git
git clone git@gitlab.com:cfinan/stdopen.git
cd stdopen
```

Install dependencies:
```
python -m pip install --upgrade -r requirements.txt
```

Then run (or add `-e` for developer install):
```
python -m pip install .
```

### Installation using conda
A conda build is also available for Python v3.8/v3.9/v.10 on linux-64/osX-64. Although please note that all development and testing is performed in linux-64.
```
conda install -c conda-forge -c cfin stdopen
```

If you are using conda and require anything different then please install with pip and install the dependencies with the environments specified in `resources/conda/env/py3*`.

## Next steps...
If you want to run the tests after install you will need to have cloned the repo and run ``pytest ./tests``.

## Change log

### version `0.2.0a0`
* API - Added the ability to pass through iterators rather than trying to process like a file path.
