.. stdopen documentation master file.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to stdopen
==================

`stdopen <https://gitlab.com/cfinan/stdopen>`_ is a small utilility to handle the logic of either opening from a file or from STDIN/STDOUT. It also provides the ability to write to output via a text file.

Contents
========

.. toctree::
   :maxdepth: 2
   :caption: Setup

   getting_started

.. toctree::
   :maxdepth: 2
   :caption: Example code

   examples

.. toctree::
   :maxdepth: 2
   :caption: Programmer reference

   api

.. toctree::
   :maxdepth: 2
   :caption: Project admin

   contributing

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
