"""Fixtures (re-usable data components and settings) for tests.
"""
import pytest
import tempfile
import os


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@pytest.fixture()
def tmpfile(tmpdir):
    """Return a temp file object, temp dir and tempfile basename.
    """
    fobj, tmpf = tempfile.mkstemp(dir=tmpdir)
    os.close(fobj)
    return tmpf, tmpdir, os.path.basename(tmpf)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@pytest.fixture()
def testdata():
    """Return a text string.
    """
    return "this is a test"
